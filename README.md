# Symbol based MG solver for DG-FEM diffusion

## Dependencies 
* Utopia 
* Petsc 
* PetIGA

## Instructions to compile and run the test 

Make sure that the installation path in the first line of bin/Makefile is correct.

```
cd bin
make 
```
