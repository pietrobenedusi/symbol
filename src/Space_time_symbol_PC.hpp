#ifndef Symbol_PC_hpp
#define Symbol_PC_hpp

#include "Space_time_assembler.hpp"
#include "Tensor_solverPC.hpp"
#include "Utilities.hpp"
#include <vector>

typedef struct {
	Space_time_assembler *st_op; 
	
	std::vector<Mat> interpolation_operators; 

	int PCG_smoothing_steps;

	KSP space_solver;     

	// this are needed by the tensor solver (one for each dimension)
	Mat *M1;
	Mat *M2;
	Mat *M3;

} SymbolShellPC;

extern PetscErrorCode SymbolShellPCCreate(SymbolShellPC **shell);
extern PetscErrorCode SymbolShellPCSetUp(PC pc, Space_time_assembler *st_op_, const int levels = 1, Mat *M1_ = NULL, Mat *M2_ = NULL, Mat *M3_ = NULL);
extern PetscErrorCode SymbolShellPCApply(PC pc,Vec x,Vec y);
extern PetscErrorCode SymbolShellPCDestroy(PC pc);

#endif 
