// #include "Space_time_assembler.hpp"
// #include "Utilities.hpp"
#include "Space_time_symbol_PC.hpp"
#include "Tensor_solverPC.hpp"
//#include "Space_timeMG.hpp"
#include <ctime>
#include <iomanip>


#define pi 3.1415926535897932
#define r0 1.0
#define r1 2.0

using namespace utopia;

void coupling_q(const int time_nodes_fine, PetscMatrix &interp_op)
{
        
    switch (time_nodes_fine)
    {

        case 1:
            
            {            
                auto ml = serial_layout(1, 1);                        
                interp_op.dense(ml,1.);                    
                break;
            }                     

        break;


        case 2:
        
            interp_op = zeros(2,1);   
            {
                Write<PetscMatrix> w(interp_op);
                interp_op.set(0,0, 1.0);                                                           
                interp_op.set(1,0, 1.0);
            }            

        break;

        case 3:

            interp_op = zeros(3,2);   
            {
                Write<PetscMatrix> w(interp_op);
                interp_op.set(0,0, 1.0);              
                interp_op.set(1,0, 0.532576538582523);                  
                interp_op.set(1,1, 0.467423461417477);                
                interp_op.set(2,1, 1.0);
            }                     
            
        break;

        case 4:

            interp_op = zeros(4,3);   
            {
                Write<PetscMatrix> w(interp_op);
                interp_op.set(0,0, 1.0);                                
                interp_op.set(1,0, 0.480675843880085);
                interp_op.set(1,1, 0.519324156119915);                            
                interp_op.set(2,1, 0.598056399942927);
                interp_op.set(2,2, 0.401943600057073);                
                interp_op.set(3,2, 1.0);
            }                     
            
        break;

        case 5:

            interp_op = zeros(5,4);   
            {
                Write<PetscMatrix> w(interp_op);
                interp_op.set(0,0, 1.0);              
                interp_op.set(1,0, 0.413314333743307);  
                interp_op.set(1,1, 0.586685666256693);                                
                interp_op.set(2,1, 0.539590226879033);                
                interp_op.set(2,2, 0.460409773120967);                                
                interp_op.set(3,2, 0.658187388535170);                
                interp_op.set(3,3, 0.341812611464830);                
                interp_op.set(4,3, 1.0);
            }                     
            
        break;

        default:
            std::cerr << "ERROR: Order in time greater then 5 not supported!" << std::endl;    
            std::cerr << "Current order = " << time_nodes_fine << std::endl;            
        break;
    }
}

double set_rhs_function(const std::vector<PetscReal> point) {
	// return -2*3.1415926536*exp(point[0])*(cos(2*3.1415926536*point[0]) - 2*3.1415926536*sin(2*3.1415926536*point[0]));
	// return -2*3.1415926536*exp(point[0])*(cos(2*3.1415926536*point[0]) - 2*3.1415926536*sin(2*3.1415926536*point[1]));

	
	// const double pi = 3.1415926535897932;

	// return  cos(pi * point[0]) + 2 * cos(3 * pi * point[0]) + 3 * cos(4 * pi * point[0]);

	return 1.;
}

// std::vector<std::vector<PetscReal> >  set_coefficients(const std::vector<PetscReal> point) {
// 	std::vector<std::vector<PetscReal> > coeff = {{0,0,0},{0,0,0},{0,0,0}};
// 	coeff[0][0] = 1; //cos(point[0]) + point[1]; //(2 + cos(point[0])) * (1 + point[1]);;//cos(point[0]) + point[1]; //(2 + cos(point[0])) * (1 + point[1]); //cos(point[0]) + point[1]; //(2 + cos(point[0])) * (1 + point[1]);
// 	coeff[0][1] = 0; //(cos(point[0] + point[1])) * (sin(point[0] + point[1]));
// 	coeff[0][2] = 0;
// 	coeff[1][0] = 0; //(cos(point[0] + point[1])) * (sin(point[0] + point[1]));
// 	coeff[1][1] = 1;//point[0] + sin(point[1]); //(2 + sin(point[1])) * (1 + point[0]); //point[0] + sin(point[1]); //(2 + sin(point[1])) * (1 + point[0]); 
// 	coeff[1][2] = 0;
// 	coeff[2][0] = 0;
// 	coeff[2][1] = 0;
// 	coeff[2][2] = 1;
// 	return coeff;
// }


double set_geometry_jacobian(const std::vector<PetscReal> point) {

    return (pi * abs((r1 - r0) * (r0 + r1 * point[0]- r0 * point[0])))/2.0;
    
    // if the domain is [0,1]^d, return 1 
    // return 1.;
}

std::vector<std::vector<PetscReal> >  set_coefficients(const std::vector<PetscReal> point) {
        
    std::vector<std::vector<PetscReal> > coeff = {{0,0,0},{0,0,0},{0,0,0}};
    coeff[0][0] = (set_geometry_jacobian(point))/pow(r1 - r0,2);
    coeff[0][1] = 0;
    coeff[0][2] = 0;
    coeff[1][0] = 0;
    coeff[1][1] = 4 * (set_geometry_jacobian(point))/pow(pi * (r0 + r1 * point[0] - r0 * point[0]),2); 
    coeff[1][2] = 0;
    coeff[2][0] = 0;
    coeff[2][1] = 0;
    coeff[2][2] = 1;
    return coeff;
}
 

///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


// just space solve, MG with PETSC // works just in serial becasue K is serialized
// int main(int argc, char** argv) 
// {
// 	Utopia::Init(argc, argv);
// 	PetscErrorCode ierr;	

//     if (mpi_world_rank() > 1) 
//     {
//         std::cout << "ERROR: this test works just in serial." << std::endl;
//         return 1;
//     }
	   	        
//     // Space assembling       
//     const int p = atoi(argv[1]); 
//     const int k = p - 1;
//     const int n = 131 - p;    
//     const int levels = 5;
//     const int s_p = atoi(argv[2]); 

//     std::clock_t start = std::clock();   

//     Space_assembler s_ass({p,p},{k,k},{n,n},set_rhs_function,set_coefficients); 
//     auto s_ass_ptr = std::make_shared<Space_assembler>(s_ass);

//     std::cout << "space size =  " << s_ass_ptr->get_size() << std::endl;
//     std::cout << "n = " << n << std::endl;
//     std::cout << "p = " << p << std::endl;
//     std::cout << "k = " << k << std::endl;
//     std::cout << "levels = " << levels << std::endl;
//     std::cout << "s_p = " << s_p << std::endl;

//     std::cout << "Assemble problem time: " << ( std::clock() - start ) / (double) CLOCKS_PER_SEC << std::endl;

//     // space petsc MG solver test 
//     KSP space_solver;       

//     ierr = KSPCreate(PETSC_COMM_WORLD,&space_solver);CHKERRQ(ierr); 

//     // if (levels == 0)
//     // {
//     //     ierr = KSPSetOperators(space_solver, raw_type(*s_ass_ptr->get_stiff()), raw_type(*s_ass_ptr->get_stiff()));CHKERRQ(ierr);    
//     //     ierr = KSPSetType(space_solver,KSPCG);CHKERRQ(ierr);    
//     // }
//     // else
//     // {
//     ierr = KSPSetOperators(space_solver, raw_type(*s_ass_ptr->get_stiff()), raw_type(*s_ass_ptr->get_stiff()));CHKERRQ(ierr);    
//     ierr = KSPSetType(space_solver,KSPCG);CHKERRQ(ierr);        
//     // }
    
//     ierr = KSPSetTolerances(space_solver,1e-8,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);               
//     ierr = KSPSetNormType(space_solver,  KSP_NORM_UNPRECONDITIONED );CHKERRQ(ierr);  
//     ierr = KSPSetFromOptions(space_solver);CHKERRQ(ierr);         
//     ierr = KSPSetUp(space_solver);CHKERRQ(ierr); 

//     // Mat K_coarse;

//     if (levels > 0)
//     {            
//         start = std::clock();        

//         // set interpolation operators
//         std::vector<Mat> interpolation_operators;
//         interpolation_operators.reserve(levels);

//         int dim = s_ass_ptr->get_dim();
//         int size = s_ass_ptr->get_size(); // for now MG works for square domain, not rectangular
//         int n_fine = (int) pow(size, 1./dim);        

//         for (int i = 0; i < levels - 1; ++i)
//         {
//             Mat I_space;

//             // number of grid points for level l
//             int n_l = 1 + (n_fine - 1)/pow(2, levels - 2 - i); 
            
//             ierr = assemble_interpolation(n_l, dim, I_space);CHKERRQ(ierr);
                
//             // store the interp. operators, coarsest is in 0 
//             interpolation_operators.push_back(I_space);

//             // ierr = MatDestroy(&I_space);CHKERRQ(ierr);            
//         }

//         PC pcmg;
//         ierr = KSPGetPC(space_solver, &pcmg);CHKERRQ(ierr);
//         ierr = PCSetType(pcmg, PCMG);CHKERRQ(ierr);        
//         ierr = PCMGSetLevels(pcmg,levels, NULL);CHKERRQ(ierr);
        
//         // ierr = PCMGSetType(pcmg,PC_MG_KASKADE);CHKERRQ(ierr);    
//         // ierr = PCMGSetGalerkin(pcmg,PC_MG_GALERKIN_NONE);CHKERRQ(ierr);         
//         ierr = PCMGSetGalerkin(pcmg,PC_MG_GALERKIN_BOTH);CHKERRQ(ierr); 

//         // smoothers    
//         // for (int i = levels - 1; i >= 0; --i)  
//         for (int i = 1; i < levels; ++i)
//         {
//             KSP smoother;
//             ierr = PCMGGetSmoother(pcmg, i, &smoother);CHKERRQ(ierr);        

//             // if (i == levels - 1) // fine level
//             // {
//             //     ierr = KSPSetType(smoother, KSPCG);CHKERRQ(ierr);                        
//             //     ierr = KSPSetOperators(smoother, raw_type(*s_ass_ptr->get_stiff()), raw_type(*s_ass_ptr->get_mass()));CHKERRQ(ierr);                                                         

//             //     ierr = KSPSetTolerances(smoother,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, 0);CHKERRQ(ierr);               
//             //     // ierr = KSPSetNormType(smoother,  KSP_NORM_UNPRECONDITIONED );CHKERRQ(ierr);  //for debug
                
//             //     ierr = KSPSetNormType(smoother, KSP_NORM_NONE);CHKERRQ(ierr);  

//             //     // PC sm;
//             //     // ierr = KSPGetPC(smoother, &sm);CHKERRQ(ierr);            
//             //     // ierr = PCSetType(sm,    PCILU);CHKERRQ(ierr);                   
//             // }
//             // else
//             // {             
//             //     if ( i == levels - 2) // first time          // TODO improve PETSC_DEFAULT 
//             //     {
//             //         ierr = MatPtAP(raw_type(*s_ass_ptr->get_stiff()), interpolation_operators[i], MAT_INITIAL_MATRIX, PETSC_DEFAULT, &K_coarse);CHKERRQ(ierr); 
//             //     }
//             //     else
//             //     {
//             //         ierr = MatPtAP(K_coarse, interpolation_operators[i], MAT_INITIAL_MATRIX, PETSC_DEFAULT, &K_coarse);CHKERRQ(ierr); 
//             //     }
                
//             //     ierr = KSPSetOperators(smoother, K_coarse, K_coarse);CHKERRQ(ierr);                                                   
//             // }                            

//             // if (i > 0 && i < levels - 1) // nor coarser or finer
//             // if (i > 0)
//             // {        
//                 // G-S smoother
//                 ierr = KSPSetType(smoother, KSPRICHARDSON);CHKERRQ(ierr); 

//                 PC sm;
//                 ierr = KSPGetPC(smoother, &sm);CHKERRQ(ierr);            
//                 ierr = PCSetType(sm, PCSOR   );CHKERRQ(ierr);                 

//                 if (i == levels - 1)
//                 {
//                 	ierr = KSPSetTolerances(smoother,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, s_p);CHKERRQ(ierr);     
//                 }
//                 else
//                 {
//                 	ierr = KSPSetTolerances(smoother,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, 1);CHKERRQ(ierr);     
//                 }
                
//                 ierr = KSPSetNormType(smoother, KSP_NORM_NONE);CHKERRQ(ierr);                                        
//             // }        

//             // if (i > 0)
//             // {
//                 // set interpolators
//                 ierr = PCMGSetInterpolation(pcmg, i, interpolation_operators[i-1]);CHKERRQ(ierr);  

//             // }
//         }

//         std::cout << "Set up MG time: " << ( std::clock() - start ) / (double) CLOCKS_PER_SEC << std::endl;        
//     }
      
//     PetscVector x_out = zeros(s_ass_ptr->get_size());
//     PetscVector rhs = values(s_ass_ptr->get_size(), 1.);
  	   
//     start = std::clock();  
    
//     ierr = KSPSolve(space_solver, raw_type(rhs), raw_type(x_out));CHKERRQ(ierr);	  

//     std::cout << "Solve time: " << ( std::clock() - start ) / (double) CLOCKS_PER_SEC << std::endl;
    
//     // print infos
//     print_KSP_infos(space_solver);

//     // disp(x_out);
 
//     // clean
//     ierr = KSPDestroy(&space_solver);CHKERRQ(ierr);     

// 	return 0; 	
// }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// complete symbol PGMRES, for example execute with: make -j64 main && srun -n 10 ./main 3 30 5 5 -ksp_monitor
int main(int argc, char** argv) 
{	
	Utopia::Init(argc, argv);
	PetscErrorCode ierr;
	int rank;
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );

	// input params
	const int prec_type = atoi(argv[1]); // PGMRES type 

    // 0 no P
    // 1 BJ-ILU on original system
    // 2 BJ-ILU on symbol P
    // 3 PCSHELL on symbol P

	// Space assembling params	
	const int p = atoi(argv[2]);
    const int k = p - 1;
	const int n = 131 - p; //atoi(argv[2]);	     // dofs in one dimension
	

	// if (n < mpi_world_size() && rank == 0)
	// {
	// 	std::cout << "ERROR: n too small for mpi size!" << std::endl;
	// 	return 0;
	// }

	// space mg levels
	const int MG_levels = 5; //atoi(argv[4]); //(int)floor(log2(n + k - 1)) - 3; 

	// time assembly parameters
	const int q = 1;
	const double T = 1.;
	const int time_steps = 20;
	const double dt = T/time_steps; 		
	    	
	// set flags 
	bool tensor_solver_flag = 0;
	
	// assemble precondioner depending on prec_typ1
	bool assemble_prec = false;
	if (prec_type == 2 || prec_type == 3)
	{
		assemble_prec = true;
	}	 

	std::clock_t start = std::clock();    
	
    if (rank == 0) {
        
        std::cout << "===== Starting simulation =====" << std::endl;
        std::cout << "MPI size = " << mpi_world_size() << std::endl;
        std::cout << "p = " << p << std::endl;
        std::cout << "k = " << k << std::endl;
        std::cout << "q = " << q << std::endl;
        std::cout << "n = " << n << std::endl;
        std::cout << "time_steps = " << time_steps << std::endl;
        std::cout << "prec_type = " << prec_type << std::endl;
        if (prec_type == 3) std::cout << "MG_levels = " << MG_levels << std::endl;
        std::cout << "==============================="<< std::endl;
    }
    
    std::clock_t start_space = std::clock();    
    double duration;

	// Space_assembler s_ass({p,p},{k,k},{n,n},set_rhs_function,set_coefficients);	
    Space_assembler s_ass({p,p},{k,k},{n,n},set_rhs_function,set_geometry_jacobian,set_coefficients);   
	auto s_ass_ptr = std::make_shared<Space_assembler>(s_ass);

	if (rank == 0)
	{
		std::cout << "Space operators assembled, size = " << s_ass_ptr->get_size() << std::endl;

		duration = ( std::clock() - start_space ) / (double) CLOCKS_PER_SEC;
		std::cout << "Space assembling time =  "<< duration <<'\n';		
	}
		
    if (prec_type == 3 && rank == 0)
    {
        std::cout << "Suggestions for n for MG = ";
        for (int i = 4; i < 12; ++i)
        {
            std::cout << pow(2,i) - k + 2 << ", ";
        }
        std::cout << std::endl;
    }
    
	// Time assembling
	auto t_ass_ptr = std::make_shared<Time_assembler>(dt, q);

	// Space-time assembling

	std::clock_t start_mid = std::clock();

	Space_time_assembler st_ass(s_ass_ptr, t_ass_ptr, time_steps, assemble_prec); 
	// st_ass.set_rhs(1.);

	if (rank == 0)
	{
		std::cout << "Space-time operators assembled" << std::endl;
	
		duration = ( std::clock() - start_mid ) / (double) CLOCKS_PER_SEC;
		std::cout << "Space-time assembling time =  "<< duration <<'\n';		
	
		duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
		std::cout << "Total assembling time =  "<< duration <<'\n';
	}
    
	start_mid = std::clock(); 

	// Solve
	PetscVector x_0 = 0. * (*st_ass.get_space_time_rhs());	

	KSP solver;      		

    ierr = KSPCreate(PETSC_COMM_WORLD,&solver);CHKERRQ(ierr);

    if (prec_type == 2 || prec_type == 3) 
    {    
    	ierr = KSPSetOperators(solver,raw_type(*st_ass.get_space_time_system()),raw_type(*st_ass.get_space_time_system_prec()));CHKERRQ(ierr);	    		
    }
    else{
    	ierr = KSPSetOperators(solver,raw_type(*st_ass.get_space_time_system()),raw_type(*st_ass.get_space_time_system()));CHKERRQ(ierr);
    }
    
    ierr = KSPSetType(solver,KSPGMRES);CHKERRQ(ierr);
    
    PC pc;   
    KSPGetPC(solver,&pc);    
 		   	
	if (prec_type == 0)
	{	   				  
    	PCSetType(pc,PCNONE);
	}   
	else if (prec_type == 2 || prec_type == 1)
	{
		// PCSetType(pc,PCLU);	  // Default is block jacobi with ILU
	}
	else if (prec_type == 3)
	{
		SymbolShellPC  *shell;

		PCSetType(pc,PCSHELL);
		SymbolShellPCCreate(&shell);				
		PCShellSetApply(pc,SymbolShellPCApply);				
		PCShellSetContext(pc,shell);				
		PCShellSetDestroy(pc,SymbolShellPCDestroy);	

		if (tensor_solver_flag) // tensor solver for mass matrix
		{				
			// if (p == 1)
			// {
			// 	Space_assembler s_ass_aux({p},{k},{n},set_rhs_function,set_coefficients);					
			// 	make_toeplitz(raw_type(*s_ass_aux.get_mass()), p);	
			// 	SymbolShellPCSetUp(pc,&st_ass, MG_levels,  &raw_type(*s_ass_aux.get_mass()),  &raw_type(*s_ass_aux.get_mass()));				
			// }
			// else{
			// 	Space_assembler s_ass_aux({p-1},{k-1},{n+1},set_rhs_function,set_coefficients);					
			// 	make_toeplitz(raw_type(*s_ass_aux.get_mass()), p);	
			// 	SymbolShellPCSetUp(pc,&st_ass, MG_levels,  &raw_type(*s_ass_aux.get_mass()),  &raw_type(*s_ass_aux.get_mass()));				
			// }
		}
		else{
			SymbolShellPCSetUp(pc,&st_ass, MG_levels);				
		}		
	}

    // free memory
    ierr = MatDestroy(&raw_type(*s_ass.get_mass()));CHKERRQ(ierr);
    ierr = MatDestroy(&raw_type(*s_ass.get_stiff()));CHKERRQ(ierr);

    ierr = KSPSetTolerances(solver,1e-8,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);   
    // ierr = KSPSetNormType(solver,  KSP_NORM_UNPRECONDITIONED );CHKERRQ(ierr);              		        
    ierr = KSPSetFromOptions(solver);CHKERRQ(ierr);		    
    // ierr = KSPSetUp(solver);CHKERRQ(ierr); // ------------------------> TODO remove?

    // set up solver time 
    if (rank == 0)
	{
		duration = ( std::clock() - start_mid ) / (double) CLOCKS_PER_SEC;
		std::cout << "Setup time =  "<< duration <<'\n'; 
	}

	start_mid = std::clock(); 
       
    ierr = KSPSolve(solver,raw_type(*st_ass.get_space_time_rhs()), raw_type(x_0));CHKERRQ(ierr);
    
    // print infos
    if (rank == 0)
	{
		// print PC type info
		PCType type;
    	PCGetType(pc, &type);
		std::cout << "PC type = " << type << std::endl;

		int its;			    
		KSPConvergedReason reason;

		KSPGetConvergedReason(solver,&reason);

		std::cout << "reason: " << reason << std::endl;
	    
	    if (reason == KSP_DIVERGED_INDEFINITE_PC) 
	    {		    	
    		PetscPrintf(PETSC_COMM_WORLD,"\nDivergence because of indefinite preconditioner;\n");
    		PetscPrintf(PETSC_COMM_WORLD,"Run the executable again but with '-pc_factor_shift_type POSITIVE_DEFINITE' option.\n");
		} 
		else if (reason<0) {
			PetscPrintf(PETSC_COMM_WORLD,"\nOther kind of divergence: this should not happen.\n");
			KSPGetIterationNumber(solver,&its);
			std::cout << "Iterations = " << its << std::endl;
		} else {
			KSPGetIterationNumber(solver,&its);
			std::cout << "Iterations = " << its << std::endl;
		}
	}

    ierr = KSPDestroy(&solver);CHKERRQ(ierr);  
    		   
	// print total times     		    
	if (rank == 0)
	{
		duration = ( std::clock() - start_mid ) / (double) CLOCKS_PER_SEC;
		std::cout << "Solve time =  "<< duration <<'\n';
			
		duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

	    std::cout << "Total time =  "<< duration <<'\n';
	    std::cout << "Total #dofs = " << st_ass.get_size() << std::endl;
	}



	// output
	// save_vec(raw_type(x_0), "../mat_files/sol_d.m", "sol_daint");
	// save_mat(raw_type(*st_ass.get_time_approx()), "../mat_files/M_tilda_daint.m", "M_daint");
	// save_mat(raw_type(*st_ass.get_space_time_system()), "../mat_files/C_seq.m", "C_d_seq");
	// save_mat(raw_type(*st_ass.get_space_time_system_prec()), "../mat_files/P_d.m", "P_daint");
	// save_vec(raw_type(*st_ass.get_space_time_rhs()), "../mat_files/rhs_d.m", "rhs_daint");
    
	return 0; 	
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// // space-time MG for PFASST comparison

// int main(int argc, char** argv) 
// {
// 	Utopia::Init(argc, argv);
// 	PetscErrorCode ierr;
// 	int rank;
// 	MPI_Comm_rank( MPI_COMM_WORLD, &rank );

// 	// input params
	
// 	// Space assembling params
// 	const int p = 1;	     // dofs in one dimension
// 	const int n = 1026;
// 	const int k = p - 1;	

// 	if (n < mpi_world_size() && rank == 0) std::cout << "WARNING: n may be too small for mpi size!" << std::endl;

	
// 	// time assembly parameters
// 	const int time_steps = atoi(argv[1]);
// 	const int q = atoi(argv[2]);
// 	const double T = 1.;
// 	const double dt = T/time_steps; 

// 	// mg params
// 	const int smoothing_steps = 3;
// 	const int MG_levels = argc > 3 ? atoi(argv[3]) : 5;
// 	const bool time_coarsening      = argc > 4 ? atoi(argv[4]) : false;
// 	const bool time_elem_coarsening = argc > 5 ? atoi(argv[5]) : false;
	    
// 	std::clock_t start = std::clock();    
// 	double duration;
	
//     if (rank == 0) {
        
//         std::cout << "===== Starting simulation =====" << std::endl;
        
//         std::cout << "MPI size = " << mpi_world_size() << std::endl;
//         std::cout << "p = " << p << std::endl;
//         std::cout << "k = " << k << std::endl;
//         std::cout << "q = " << q << std::endl;
//         std::cout << "n = " << n << std::endl;
//         std::cout << "time_steps = " << time_steps << std::endl;
//         std::cout << "T = " << T << std::endl;
//         std::cout << "mu = "<< n * n * dt << std::endl;		
//         std::cout << "MG_levels = " << MG_levels << std::endl;        
//         if (time_coarsening) std::cout << "time coarsening enabled" << std::endl;        
//         if (time_elem_coarsening) std::cout << "time elem. coarsening enabled" << std::endl;          
        
//         std::cout << "==============================="<< std::endl;
//     }

// 	// Space_assembler s_ass({p,p},{k,k},{n,n},set_rhs_function,set_coefficients);	    
// 	Space_assembler s_ass({p},{k},{n},set_rhs_function,set_coefficients, true);		
    
// 	auto s_ass_ptr = std::make_shared<Space_assembler>(s_ass);

// 	// TODO mass matrix lumping /////////////////////////////////////////// ------->>>>

// 	if (rank == 0) std::cout << "Space operators assembled, size = " << s_ass_ptr->get_size() << std::endl;
	
//     if (rank == 0)
//     {
//         std::cout << "Suggestions for n for MG = ";
//         for (int i = 4; i < 12; ++i)
//         {
//             std::cout << pow(2,i) - k + 2 << ", ";
//         }
//         std::cout << std::endl;
//     }
    
// 	// Time assembling    
// 	auto t_ass_ptr = std::make_shared<Time_assembler>(dt, q);
    
// 	// Space-time assembling

// 	std::clock_t start_mid = std::clock();

// 	Space_time_assembler st_ass(s_ass_ptr, t_ass_ptr, time_steps, false); 

// 	if (rank == 0)
// 	{
// 		std::cout << "Space-time operators assembled" << std::endl;
	
// 		duration = ( std::clock() - start_mid ) / (double) CLOCKS_PER_SEC;
// 		std::cout << "Space-time assembling time =  "<< duration <<'\n';		
	
// 		duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
// 		std::cout << "Total assembling time =  "<< duration <<'\n';
// 	}

// 	start_mid = std::clock(); 

// 	// assemble rhs
// 	// rhs just in the first time slab
// 	PetscVector rhs = *st_ass.get_space_time_rhs();

// 	double pi = 3.1415926535897932;
// 	int N_x = n - 1;
// 	double dx = 1.0/N_x;

// 	{
// 	    //Get view with global indexing
// 	    auto y_view = view_device(rhs);

// 	    // Iterate over range with global indexing
// 	    parallel_for(range_device(rhs), UTOPIA_LAMBDA(const SizeType &i) {	        
// 	        if (i >= (q + 1) * N_x)
// 	        {
// 	        	y_view.set(i, 0.0);
// 	        }
// 	        else
// 	        {	 	        		       
// 	        	double x = (i % N_x) * dx;	        
// 	        	double f = - (cos(pi * x) + 2 * cos(3 * pi * x) + 3 * cos(4 * pi * x));
// 	        	y_view.set(i, f);
// 	        };
// 	    });
// 	}

// 	rhs = (*st_ass.get_time_coupling()) * rhs;

// 	// Solve
// 	PetscVector x_0 = 0. * (*st_ass.get_space_time_rhs());	

// 	KSP solver;      		

//     ierr = KSPCreate(PETSC_COMM_WORLD,&solver);CHKERRQ(ierr);
//   	ierr = KSPSetOperators(solver,raw_type(*st_ass.get_space_time_system()),raw_type(*st_ass.get_space_time_system()));CHKERRQ(ierr);    
            	
//     // MG solver set up   
//     // compute interpolation operators if needed 
//     std::vector<Mat> interpolation_operators;
//     interpolation_operators.reserve(MG_levels);
      
//     // get space #dofs
//     const int dim  = s_ass.get_dim();
//     const int size = s_ass.get_size(); 
//     const int n_fine = (int) pow(size, 1./dim);
    
//     PetscMatrix I_time_steps, I_time_elem;

//     // void coupling_q(const int time_nodes_fine, PetscMatrix &interp_op)

//     // set up interpolant
//     for (int i = 0; i < MG_levels - 1; ++i)
//     {
//         Mat I_space, I_time, I; 

//         int lev = MG_levels - 2 - i;
        
//         // time coarsening
//         if (time_coarsening)
//         {
//         	if (time_steps % 2 == 0)
//         	{
//         		int time_steps_l = time_steps/pow(2, lev);        	
//         		if (time_steps_l < 2) time_steps_l = 2;
//         		coupling_even(time_steps_l, raw_type(I_time_steps));         		
//         	}
//         	else
//         	{
//         		int time_steps_l = 1 + (time_steps - 1)/pow(2, lev); 
//         		if (time_steps_l < 3) time_steps_l = 3;   	
//         		ierr = assemble_interpolation(time_steps_l, 1, raw_type(I_time_steps));CHKERRQ(ierr);  
//         	}        	 
//         }
//         else
//         {
//         	I_time_steps = identity(time_steps,time_steps);
//         }

//         if (time_elem_coarsening)
//         {
//         	int q_l = q + 1 - lev;
//         	if (q_l < 1) q_l = 1;
        	
//         	coupling_q(q_l, I_time_elem);
//         }
//         else
//         {
//         	I_time_elem = identity(q + 1, q + 1);
//         }

//         ierr = kron(raw_type(I_time_steps), raw_type(I_time_elem), I_time);CHKERRQ(ierr);   

//         // space coarsening
//         // number of grid points for level l
//         int n_l = 1 + (n_fine - 1)/pow(2, lev);  
                
//         ierr = assemble_interpolation(n_l, dim, I_space);CHKERRQ(ierr);
        

//         if (i == MG_levels - 2) // fine level: same structure of space-time operator
//         {
//             ierr = kron(I_time, I_space, I, &raw_type(*st_ass.get_space_time_system()));CHKERRQ(ierr);           
//         }
//         else
//         {
//             ierr = kron(I_time, I_space, I);CHKERRQ(ierr);                  
//         }
        
//         // store the interp. operators, coarsest is in 0 
//         interpolation_operators.push_back(I);

//         ierr = MatDestroy(&I_space);CHKERRQ(ierr);           
//         ierr = MatDestroy(&I_time);CHKERRQ(ierr);           
//     }
    
//     // set up solver
//     ierr = KSPSetType(solver,KSPRICHARDSON);CHKERRQ(ierr);
//     PC pcmg;
//     ierr = KSPGetPC(solver, &pcmg);
//     ierr = PCSetType(pcmg, PCMG);    
//     ierr = PCMGSetLevels(pcmg,MG_levels, NULL);CHKERRQ(ierr);

//     // ierr = PCMGSetType(pcmg,PC_MG_KASKADE);CHKERRQ(ierr);    
//     ierr = PCMGSetGalerkin(pcmg,PC_MG_GALERKIN_BOTH);CHKERRQ(ierr);        
    
//     // smoothers    
//     for (int i = 1; i < MG_levels; ++i)  
//     {
//         KSP smoother;

//         ierr = PCMGGetSmoother(pcmg, i, &smoother);CHKERRQ(ierr);
      
//         ierr = KSPSetType(smoother, KSPGMRES);CHKERRQ(ierr); 
        	
//     	PC pc;   
// 		KSPGetPC(smoother,&pc);     		   		   				 
// 		PCSetType(pc,PCBJACOBI);        	
           
        
//         ierr = PCMGSetInterpolation(pcmg, i, interpolation_operators[i-1]);CHKERRQ(ierr);             
//     }

//     ierr = PCMGSetNumberSmooth(pcmg, smoothing_steps);CHKERRQ(ierr);
	

//     // final setup
// 	ierr = KSPSetFromOptions(solver);CHKERRQ(ierr);		
// 	ierr = KSPSetTolerances(solver,1e-9,1e-9,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);                   
//     // ierr = KSPSetUp(solver);CHKERRQ(ierr);

//     // set up solver time 
//     if (rank == 0)
// 	{
// 		duration = ( std::clock() - start_mid ) / (double) CLOCKS_PER_SEC;
// 		std::cout << "Setup time =  "<< duration <<'\n'; 
// 	}

// 	start_mid = std::clock(); 

// 	// solve       
//     ierr = KSPSolve(solver,raw_type(rhs), raw_type(x_0));CHKERRQ(ierr);

//     duration = ( std::clock() - start_mid ) / (double) CLOCKS_PER_SEC;
    
//     // print infos
//     if (rank == 0)
// 	{
// 		// print PC type info
// 		PCType type;
// 		PC pc;
// 	    ierr = KSPGetPC(solver, &pc);
//   		PCGetType(pc, &type);
// 		std::cout << "PC type = " << type << std::endl;

// 		int its;			    
// 		KSPConvergedReason reason;

// 		KSPGetConvergedReason(solver,&reason);

// 		std::cout << "reason: " << reason << std::endl;
	    
// 	    if (reason == KSP_DIVERGED_INDEFINITE_PC) 
// 	    {		    	
//     		PetscPrintf(PETSC_COMM_WORLD,"\nDivergence because of indefinite preconditioner;\n");
//     		PetscPrintf(PETSC_COMM_WORLD,"Run the executable again but with '-pc_factor_shift_type POSITIVE_DEFINITE' option.\n");
// 		} 
// 		else if (reason < 0) {
// 			PetscPrintf(PETSC_COMM_WORLD,"\nOther kind of divergence: this should not happen.\n");
// 			KSPGetIterationNumber(solver,&its);
// 			std::cout << "Iterations = " << its << std::endl;
// 		} else {
// 			KSPGetIterationNumber(solver,&its);
// 			std::cout << "Iterations = " << its << std::endl;
// 		}
// 	}

//     ierr = KSPDestroy(&solver);CHKERRQ(ierr);  
    		   
// 	// print total times     		    
// 	if (rank == 0)
// 	{
// 		// duration = ( std::clock() - start_mid ) / (double) CLOCKS_PER_SEC;
// 		std::cout << "Solve time =  "<< duration <<'\n';
			
// 		duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

// 	    std::cout << "Total time =  "<< duration <<'\n';
// 	    std::cout << std::scientific << "Total #dofs = " << st_ass.get_size() << std::endl;
// 	}

// 	// output
// 	// save_vec(raw_type(x_0), "../mat_files/sol_d.m", "sol_daint");
// 	// save_mat(raw_type(*st_ass.get_time_approx()), "../mat_files/M_tilda_daint.m", "M_daint");
// 	// save_mat(raw_type(*st_ass.get_space_time_system()), "../mat_files/S.m", "S_sy");
// 	// save_mat(raw_type(*st_ass.get_space_time_system_prec()), "../mat_files/P_d.m", "P_daint");
// 	// save_vec(raw_type(rhs), "../mat_files/rhs.m", "rhs_sy");
    
// 	return 0; 	
// }





































