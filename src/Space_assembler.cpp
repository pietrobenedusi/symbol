#include "Space_assembler.hpp"

typedef std::vector<PetscReal> Point;
typedef std::vector<std::vector<PetscReal>> Coefficients;  
// typedef struct {double (*f)(Point); Coefficients (*coeff)(Point);} AppContext;
typedef struct {double (*f)(Point); double (*jacobian_det)(Point); Coefficients (*coeff)(Point);} AppContext;

using namespace utopia;

// Compute the values of the entries of the Stiffness Matrix and of the RHS that are relative to the basis functions/derivatives that are not zero at a given IGAPoint
PetscErrorCode assemble_stiffness(IGAPoint p,PetscScalar *K,PetscScalar *F, void *ctx){
	
	PetscInt nen = p->nen;
	PetscInt dim = p->dim;
	Point point(p->point, p->point + sizeof p->point);
 	const PetscReal *N0	= (__typeof__(N0)) p->shape[0];
 	const PetscReal (*N1)[dim] = (__typeof__(N1)) p->shape[1];

	PetscInt a,b,i,j;
	PetscReal Ka = 0.0, Kb = 0.0;
	Coefficients  coeff = {{0,0,0},{0,0,0},{0,0,0}};
	
	AppContext *functions = (AppContext *)ctx;
	std::function<Coefficients(Point)> set_coefficients = functions->coeff;
	coeff = set_coefficients(point);
	
	// Compute Stiffness Matrix
	for (a = 0; a < nen; a++) {
		for (b = 0; b < nen; b++){
			Kb = 0.0;
			for (j = 0; j < dim; j++){
				Ka = 0.0;
				for (i = 0; i < dim; i++){					
					Ka = Ka + coeff[i][j]*N1[a][i];
				}
				Kb = Kb + Ka*N1[b][j];
			}
			K[a*nen+b] = Kb;
		}
		F[a] = N0[a] * 1.0;
	}
	return 0;
}

// Compute the values of the entries of the Mass Matrix and of the RHS that are relative to the basis functions that are not zero at a given IGAPoint
PetscErrorCode assemble_mass(IGAPoint p,PetscScalar *K,PetscScalar *F,void *ctx){
	
	PetscInt nen = p->nen;
	Point point(p->point, p->point + sizeof p->point);
	const PetscReal *N0	= (__typeof__(N0)) p->shape[0];

	PetscInt a,b;
	Coefficients  coeff = {{1,0,0},{0,0,0},{0,0,0}};
	
	AppContext *functions = (AppContext *)ctx;
	std::function<double(Point)> set_rhs = functions->f;
	std::function<double(Point)> set_geometry_jacobian = functions->jacobian_det;
	PetscReal value_rhs;
	PetscReal value_jac;
	value_jac = set_geometry_jacobian(point); 
	value_rhs = set_rhs(point) * set_geometry_jacobian(point); 

	for (a = 0; a < nen; a++) {
		for (b = 0; b < nen; b++){
			K[a*nen+b] = N0[a]*N0[b]*value_jac;
		}
		F[a] = N0[a]*value_rhs;
	}
	return 0;
}

PetscErrorCode Space_assembler::assemble(){
	
	if(!mass_)	{ mass_	= std::make_shared<PetscMatrix>(); }
	if(!stiff_) { stiff_ = std::make_shared<PetscMatrix>();}
	if(!rhs_)	{ rhs_	 = std::make_shared<PetscVector>(); }
	
	PetscErrorCode  ierr;
	
	ierr = PetscOptionsBegin(PETSC_COMM_WORLD,"","Poisson Options","IGA");CHKERRQ(ierr);
	ierr = PetscOptionsEnd();CHKERRQ(ierr);

	IGA iga;
	ierr = IGACreate(PETSC_COMM_WORLD,&iga);CHKERRQ(ierr);
	ierr = IGASetFromOptions(iga);CHKERRQ(ierr);
	ierr = IGASetDof(iga,1);CHKERRQ(ierr);
	for (unsigned int i=0; i<dim_; i++) {
        IGAAxis axis = iga->axis[i];
        PetscBool  w = axis->periodic;
        PetscInt   p_s = p_[i];
        PetscInt   N = n_per_dim_[i];
        PetscInt   C = k_[i];
        if (p_s < 1) {if (axis->p > 0) p_s = axis->p;   else p_s =  2;}
        if (N < 1) {if (axis->m > 1) N = axis->nel; else N = 5;}
		ierr = IGAAxisReset(axis);CHKERRQ(ierr);
		ierr = IGAAxisSetPeriodic(axis,w);CHKERRQ(ierr);
		ierr = IGAAxisSetDegree(axis,p_s);CHKERRQ(ierr);
		ierr = IGAAxisInitUniform(axis,N,0,1,C);CHKERRQ(ierr);
    }
	if (iga->dim < 1) {ierr = IGASetDim(iga,dim_);CHKERRQ(ierr);}
	ierr = IGASetUp(iga);CHKERRQ(ierr);

	unsigned int dir;
	PetscInt side;
	for (dir=0; dir<dim_; dir++) {
		for (side=0; side<2; side++) {
			PetscScalar value = 0.0;
			ierr = IGASetBoundaryValue(iga,dir,side,0,value);CHKERRQ(ierr);
		}
	}

	AppContext functions;
	functions.f      = f_;
	functions.jacobian_det  = jacobian_det_;
	functions.coeff  = coeff_;

	Mat K,M;
	Mat *K_seq, *M_seq;
	Vec b, b_seq;
	ierr = IGACreateMat(iga,&K);CHKERRQ(ierr);
	ierr = IGACreateMat(iga,&M);CHKERRQ(ierr);
	ierr = IGACreateVec(iga,&b);CHKERRQ(ierr);
	ierr = IGASetFormSystem(iga,assemble_stiffness,&functions);CHKERRQ(ierr);
	ierr = IGAComputeSystem(iga,K,b);CHKERRQ(ierr);
	ierr = IGASetFormSystem(iga,assemble_mass,&functions);CHKERRQ(ierr);
	ierr = IGAComputeSystem(iga,M,b);CHKERRQ(ierr);

	// init size	
	ierr = MatGetSize(M,&size_,NULL);CHKERRQ(ierr);
	
	// serialize stiff, mass and rhs
	IS is;
	ierr = ISCreateStride(PETSC_COMM_WORLD,size_,0,1,&is);CHKERRQ(ierr);
	ierr = AOApplicationToPetscIS(iga->ao,is);CHKERRQ(ierr);
	
	ierr = MatGetSubMatrices(K,1,&is,&is,MAT_INITIAL_MATRIX,&K_seq);CHKERRQ(ierr);
	ierr = MatDestroy(&K);CHKERRQ(ierr);
	ierr = MatGetSubMatrices(M,1,&is,&is,MAT_INITIAL_MATRIX,&M_seq);CHKERRQ(ierr);
	ierr = MatDestroy(&M);CHKERRQ(ierr);
	ierr = VecGetSubVector(b,is,&b_seq);CHKERRQ(ierr);
	
	// cut rows and column corresponding to boundary basis functions
	int m[2], j, counter;
	Array indices;
	if (bc_flag_)
	{
		m[0] = (p_[0] - k_[0]) * n_per_dim_[0] + k_[0] + 1;
		if (dim_!=3){m[1]=0;}
		else{m[1] = (p_[1] - k_[1]) * n_per_dim_[1] + k_[1] + 1;}
		
		counter = 0;
		for (j=0; j<size_; j++) {
			if (  (dim_ == 3 && (j<m[0]*m[1] || j>=size_-m[0]*m[1] || j%m[0]==0 || j%m[0]==m[0]-1 || j%(m[0]*m[1])<(m[0]-1) || j%(m[0]*m[1])>(m[0]*m[1]-m[0])))
				||(dim_ == 2 && (j<m[0] || j>=size_-m[0] || j%m[0]==0 || j%m[0]==m[0]-1))
				||(dim_ == 1 && (j == 0 || j == m[0] - 1))){
				counter++;
			}
			else{
				indices.resize(j-counter+1);
				indices[j-counter] = j;
			}
		}  		
		
		int indices_array[size_-counter];
		std::copy(indices.begin(), indices.end(), indices_array);
		
		Mat K_aux, M_aux;
		Vec b_aux;
		MatDuplicate(K_seq[0],MAT_COPY_VALUES,&K_aux);
		MatDuplicate(M_seq[0],MAT_COPY_VALUES,&M_aux);
		VecDuplicate(b_seq,&b_aux);
		VecCopy(b_seq,b_aux);

		ierr = ISCreateGeneral(PETSC_COMM_SELF,size_-counter,indices_array,PETSC_COPY_VALUES,&is);CHKERRQ(ierr);
		ierr = MatGetSubMatrices(K_aux,1,&is,&is,MAT_INITIAL_MATRIX,&K_seq);CHKERRQ(ierr);
		ierr = MatDestroy(&K_aux);CHKERRQ(ierr);
		ierr = MatGetSubMatrices(M_aux,1,&is,&is,MAT_INITIAL_MATRIX,&M_seq);CHKERRQ(ierr);
		ierr = MatDestroy(&M_aux);CHKERRQ(ierr);
		
		ierr = ISCreateGeneral(PETSC_COMM_WORLD,size_-counter,indices_array,PETSC_COPY_VALUES,&is);CHKERRQ(ierr);
		ierr = VecGetSubVector(b_aux,is,&b_seq);CHKERRQ(ierr);
		ierr = VecDestroy(&b_aux);CHKERRQ(ierr);
		ierr = ISDestroy(&is);CHKERRQ(ierr);
	};
	
	convert(K_seq[0],*stiff_);
	convert(M_seq[0],*mass_);
	convert(b_seq,*rhs_);
	
	// get new size	
	ierr = MatGetSize(M_seq[0],&size_,NULL);CHKERRQ(ierr);

	ierr = VecDestroy(&b);CHKERRQ(ierr);
	ierr = MatDestroyMatrices(1,&K_seq);CHKERRQ(ierr);
	ierr = MatDestroyMatrices(1,&M_seq);CHKERRQ(ierr);
	ierr = VecDestroy(&b_seq);CHKERRQ(ierr);
	ierr = IGADestroy(&iga);CHKERRQ(ierr);

	if (mass_lumping_ and mpi_world_rank() == 0)
    {
    	std::cout << "Lumping mass matrix..." << std::endl;
    	*mass_ = diag(sum(*mass_,1));
    }
	
	return ierr;
}
