#include "Space_time_assembler.hpp"
#include "Utilities.hpp"

using namespace utopia;
using std::make_shared;

void Space_time_assembler::init_sizes(){
    
	auto size_space =     space_op->get_size();
    auto size_time_slab = time_op->get_size();

    size_A = size_time_slab * size_space;
    size_ = time_steps * size_A;
}


// Assemble one-slab blocks A and B
PetscErrorCode Space_time_assembler::assemble_A_B_blocks(){ // TODO do in both ways with flag

    // init block A and B
    if(!A) { A = std::make_shared<PetscMatrix>(); }   
    if(!B) { B = std::make_shared<PetscMatrix>(); }
    if(!A_prec) { A_prec = std::make_shared<PetscMatrix>(); }    

	PetscErrorCode ierr;
    
    // A    
	ierr = kron(raw_type( *time_op->get_mass()), raw_type(*space_op->get_stiff()), raw_type(*A_prec));CHKERRQ(ierr);  
	ierr = kron(raw_type(*time_op->get_stiff()), raw_type( *space_op->get_mass()), raw_type(*A));CHKERRQ(ierr); 

	*A = *A + *A_prec;  

    // B
	ierr = kron(raw_type(*time_op->get_coupling()), raw_type(*space_op->get_mass()), raw_type(*B));CHKERRQ(ierr); 

    // Some sanity checks 
    int m_A, n_A, m_B, n_B;
    ierr = MatGetSize(raw_type(*A), &m_A, &n_A);CHKERRQ(ierr);
    ierr = MatGetSize(raw_type(*B), &m_B, &n_B);CHKERRQ(ierr);

    if (size_A != m_A)
    {
        std::cout << "WARNING: size_A != m_A!!" << std::endl;
        std::cout << "size_A = " << size_A << std::endl;
        std::cout << "m_A = " << m_A << std::endl;

        return -1;
    }

    if (m_A != m_B || n_A != n_B || n_A != m_B)
    {
        std::cerr << "WARNING: Matrices A and B does not have the same size!" << std::endl;
        return -1;
    }

    int m_A_local, m_B_local;
    ierr = MatGetLocalSize(raw_type(*A), &m_A_local, NULL);CHKERRQ(ierr);
    ierr = MatGetLocalSize(raw_type(*B), &m_B_local, NULL);CHKERRQ(ierr);

    if (m_A_local != m_B_local)
    {
        std::cerr << "WARNING: Matrices A and B does not have the same local size!" << std::endl;
        return -1;
    }

	return ierr;
}

// Assemble space-time system
PetscErrorCode Space_time_assembler::assemble(){ 
     
	assemble_A_B_blocks();
   	
    // init space-time matrix
    if(!space_time_system) { space_time_system = std::make_shared<PetscMatrix>(); }
	
	PetscErrorCode ierr;    
    PetscMPIInt    mpi_size, rank;
    MPI_Comm_size(PETSC_COMM_WORLD,&mpi_size);        
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    //Preallocation of memory     
    int nz_A = count_max_nz(raw_type(*A));
    int nz_B = count_max_nz(raw_type(*B));

    int total_nz = nz_A + nz_B; 

    // organize partioition of space time matrix dependig on A and MPI size
    int block_size = (int) ceil(((double) size_)/mpi_world_size());
    bool distribute_flag = block_size < size_A;

    std::vector<int> local_sizes;
    int local_size = 0;

    if (distribute_flag)
    {
        local_sizes = generate_distribution(size_A, size_);
        local_size = local_sizes[mpi_world_rank()];

        // check for testing
        int tot_size = 0; 
        for (int i = 0; i < mpi_world_size(); ++i){ tot_size += local_sizes[i]; }

        if (tot_size != size_ && mpi_world_rank() == 0)
        {
            std::cout << "WANRING: generate_distribution() has created incosistent local sizes!" << std::endl;
        }
    }
    
    // create the matrix trying to use an even partiotion
    ierr = MatCreate(PETSC_COMM_WORLD,&raw_type(*space_time_system));CHKERRQ(ierr); 

    if (distribute_flag) // if the block size is smaller then size_A
    {        
        ierr = MatSetSizes(raw_type(*space_time_system), local_size, local_size, size_, size_);CHKERRQ(ierr);             
    }
    else
    {        
        ierr = MatSetSizes(raw_type(*space_time_system),PETSC_DECIDE, PETSC_DECIDE, size_, size_);CHKERRQ(ierr);
    }
    
    ierr = MatSetType(raw_type(*space_time_system),MATAIJ);CHKERRQ(ierr);            

    ierr = MatSeqAIJSetPreallocation(raw_type(*space_time_system), total_nz, NULL);CHKERRQ(ierr); 
    ierr = MatMPIAIJSetPreallocation(raw_type(*space_time_system), total_nz, NULL, total_nz, NULL);CHKERRQ(ierr);     	    

    // Filling the space-time matrix
    int start, end;
    ierr = MatGetOwnershipRange(raw_type(*space_time_system), &start, &end);CHKERRQ(ierr);
    
    // copy the necessary block of A and B
    int start_A = 0;
    int block_rows;
    
    // take all block A
    if (!distribute_flag) 
    {
    	block_rows = size_A;    	
    }
    else
    {
        for (int i = 0; i < mpi_world_rank(); ++i)
        {
            start_A += local_sizes[i];
        }

        start_A = start_A % size_A;
        block_rows = local_size;
    }
    
    // get the block  
    Mat *submat_A, *submat_B;
    IS   is_row, is_col;
	
    ierr = ISCreateStride(PETSC_COMM_SELF, block_rows, start_A, 1, &is_row);CHKERRQ(ierr); 
    ierr = ISCreateStride(PETSC_COMM_SELF, size_A, 0, 1, &is_col);CHKERRQ(ierr); 
    
    ierr = MatGetSubMatrices(raw_type(*A), 1, &is_row, &is_col, MAT_INITIAL_MATRIX, &submat_A);CHKERRQ(ierr);  
    ierr = MatGetSubMatrices(raw_type(*B), 1, &is_row, &is_col, MAT_INITIAL_MATRIX, &submat_B);CHKERRQ(ierr);  

    // assemble space-time matrix 
    PetscInt ncols_A , ncols_B;
    const PetscInt    *cols_A, *cols_B;
    const PetscScalar *vals_A, *vals_B; 

    int counter = 0;

    for (int i = start; i < end; ++i)
    {    
        int i_cyclic;    
        
        if (distribute_flag)
        {
            i_cyclic = counter % size_A;
        }
        else
        {
            i_cyclic = i % size_A;
        }
        
        // set A
        ierr = MatGetRow(submat_A[0],i_cyclic,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);

        for (int j = 0; j < ncols_A; ++j)
        {
            int col_new = size_A*(i/size_A) + cols_A[j]; 

            ierr = MatSetValues(raw_type(*space_time_system),1,&i,1,&col_new,&vals_A[j],INSERT_VALUES);CHKERRQ(ierr);             
        }

        ierr = MatRestoreRow(submat_A[0],i_cyclic,&ncols_A,&cols_A,&vals_A);CHKERRQ(ierr);


        // set B 
        if (i >= size_A )
        {
            ierr = MatGetRow(submat_B[0],i_cyclic,&ncols_B,&cols_B,&vals_B);CHKERRQ(ierr);

            for (int j = 0; j < ncols_B; ++j)
            {
                int col_new = size_A*(i/size_A - 1) + cols_B[j];

                ierr = MatSetValues(raw_type(*space_time_system),1,&i,1,&col_new,&vals_B[j],INSERT_VALUES);CHKERRQ(ierr);             
            }

            ierr = MatRestoreRow(submat_B[0],i_cyclic,&ncols_B,&cols_B,&vals_B);CHKERRQ(ierr);
        }

        counter++;
    }
    
    // clean
    ierr = MatDestroy(&submat_A[0]);CHKERRQ(ierr);
    ierr = PetscFree(submat_A);CHKERRQ(ierr);
    ierr = MatDestroy(&submat_B[0]);CHKERRQ(ierr);
    ierr = PetscFree(submat_B);CHKERRQ(ierr);
    ierr = ISDestroy(&is_row);CHKERRQ(ierr);
    ierr = ISDestroy(&is_col);CHKERRQ(ierr);

    ierr = MatAssemblyBegin(raw_type(*space_time_system),MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(raw_type(*space_time_system),MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);   

    // optimize_nnz(*space_time_system); 

    // A and B are freed here!
    ierr = MatDestroy(&raw_type(*A));CHKERRQ(ierr);
    ierr = MatDestroy(&raw_type(*B));CHKERRQ(ierr);

    return ierr;
}

// I_t kron M_t kron M_s
PetscErrorCode Space_time_assembler::assemble_rhs(){

    //TODO  this routine may contain a small bug FIXME

    PetscErrorCode ierr;

    if(!space_time_rhs) { space_time_rhs = std::make_shared<PetscVector>(); }
    
    auto time_slabs_mass = std::make_shared<PetscMatrix>();

    PetscMatrix time_id  = identity(time_steps,time_steps);
    PetscMatrix space_id = identity(space_op->get_mass()->size());

    Mat Mass_slab;

    ierr = kron(raw_type(*time_op->get_mass()), raw_type(space_id), Mass_slab);CHKERRQ(ierr); 

    // MPI_Barrier(MPI_COMM_WORLD);
    // std::cout << "done 1"<< std::endl;    
    
    ierr = kron(raw_type(time_id), Mass_slab, raw_type(*time_slabs_mass), &raw_type(*space_time_system));CHKERRQ(ierr); 

    // MPI_Barrier(MPI_COMM_WORLD);
    // std::cout << "done 2"<< std::endl;    

    // clean
    ierr = MatDestroy(&Mass_slab);CHKERRQ(ierr);

    // fill rhs    
    auto rhs_space      = space_op->get_rhs();
    auto size_time_slab = time_op->get_size();

    // local range 
    Range rows_ = row_range(*space_time_system); 

    std::vector<PetscVector::SizeType> index;
    index.reserve(rows_.extent());

    // copy spatial rhs accordin to the space-time indexing    
    for (int i = rows_.begin(); i < rows_.end(); i++)  
    {               
        index.push_back(i % (size_A/size_time_slab));
    }

    // assign rhs
    rhs_space->select(index, *space_time_rhs);

    // scale by mass
    *space_time_rhs = (*time_slabs_mass)*(*space_time_rhs);

    return ierr; 
}

// assemble space-time preconditioner  
PetscErrorCode Space_time_assembler::assemble_preconditioner()
{
    PetscErrorCode ierr;

    // init
    if(!space_time_system_prec) { space_time_system_prec = std::make_shared<PetscMatrix>(); }
            
    PetscPrintf(PETSC_COMM_WORLD,"Begin: assemble_preconditioner()...\n");
    
    PetscMatrix I_time = identity(time_steps,time_steps); 
    ierr = kron(raw_type(I_time), raw_type(*A_prec), raw_type(*space_time_system_prec), &raw_type(*space_time_system));CHKERRQ(ierr);
    
    // ierr = MatChop(raw_type(*space_time_system_prec), 1e-11);CHKERRQ(ierr);
    // optimize_nnz(*space_time_system_prec); 

    ierr = MatDestroy(&raw_type(*A_prec));CHKERRQ(ierr);

    PetscPrintf(PETSC_COMM_WORLD,"End: assemble_preconditioner()...\n");
    
    return ierr;
}


void Space_time_assembler::set_space_approx(){

    PetscErrorCode  ierr;

    // init
    if(!space_approx) { space_approx = std::make_shared<PetscMatrix>(); }
    
    int time_size = time_steps * (time_op->get_size());
    
    // construct the operator diag(space_stiff) 
    PetscMatrix I_time = identity(time_size,time_size);

    ierr = kron(raw_type(I_time), raw_type(*space_op->get_stiff()), raw_type(*space_approx), &raw_type(*space_time_system));CHKERRV(ierr);  
}

void Space_time_assembler::set_space_mass_approx(){

    PetscErrorCode  ierr;

    // init
    if(!space_mass_approx) { space_mass_approx = std::make_shared<PetscMatrix>(); }
    
    int time_size = time_steps * (time_op->get_size());
    
    // construct the operator diag(space_stiff) 
    PetscMatrix I_time = identity(time_size,time_size);

    ierr = kron(raw_type(I_time), raw_type(*space_op->get_mass()), raw_type(*space_mass_approx), &raw_type(*space_time_system));CHKERRV(ierr);  
}


void Space_time_assembler::set_time_approx(){

    PetscErrorCode  ierr;

    // init
    if(!time_approx) { time_approx = std::make_shared<PetscMatrix>(); }

    int space_size = space_op->get_size();

    PetscMatrix I_time_steps = identity(time_steps,time_steps);
    PetscMatrix I_space = identity(space_size,space_size);

    Mat M_tmp;

    // assemble I_t kron inv(M_t) kron Ix 
    ierr = kron(raw_type(I_time_steps), raw_type(*time_op->get_mass_inv()), M_tmp);CHKERRV(ierr);
    ierr = kron(M_tmp, raw_type(I_space), raw_type(*time_approx), &raw_type(*space_time_system));CHKERRV(ierr);   

    ierr = MatDestroy(&M_tmp);CHKERRV(ierr);    
}

void Space_time_assembler::set_time_coupling(){

    PetscErrorCode  ierr;

    // init
    if(!time_coupling) { time_coupling = std::make_shared<PetscMatrix>(); }

    int space_size = space_op->get_size();

    PetscMatrix I_time_steps = identity(time_steps,time_steps);
    
    Mat M_tmp;

    // assemble I_t kron inv(M_t) kron Ix 
    ierr = kron(raw_type(I_time_steps), raw_type(*time_op->get_coupling()), M_tmp);CHKERRV(ierr);
    ierr = kron(M_tmp, raw_type(*space_op->get_mass()), raw_type(*time_coupling), &raw_type(*space_time_system));CHKERRV(ierr);   

    ierr = MatDestroy(&M_tmp);CHKERRV(ierr);    
}

// rhs = (I_t kron M_t kron M_x) * value_, considering BC
void Space_time_assembler::set_rhs(const double value_){

    // {
    //     ReadAndWrite<PetscVector> rw_t(*space_time_rhs);
    //     auto r = range(*space_time_rhs );
    //     for(auto i = r.begin(); i != r.end(); ++i) {
    //         if(space_time_rhs->get(i) != 0) {
    //             space_time_rhs->set(i, value_);
    //         }
    //     }
    // }

    // PetscMatrix I_time_steps = identity(time_steps,time_steps);

    // Mat M_tmp;
    // PetscMatrix space_time_mass;

    // PetscErrorCode  ierr;

    // // assemble (I_t kron M_t kron M_x) 
    // ierr = kron(raw_type(I_time_steps), raw_type(*time_op->get_mass()), M_tmp);CHKERRV(ierr);
    // ierr = kron(M_tmp, raw_type(*space_op->get_mass()), raw_type(space_time_mass), &raw_type(*space_time_system));CHKERRV(ierr); 

    // *space_time_rhs = space_time_mass * (*space_time_rhs);

    // ierr = MatDestroy(&M_tmp);CHKERRV(ierr);   
    // ierr = MatDestroy(&raw_type(space_time_mass));CHKERRV(ierr);   



    // prova

    
                    

    // just initial condition
    {
        ReadAndWrite<PetscVector> rw_t(*space_time_rhs);
        auto r = range(*space_time_rhs);
        for(auto i = r.begin(); i != r.end(); ++i) {
            if(space_time_rhs->get(i) != 0 && i < size_A) {
                space_time_rhs->set(i, 0);
            }
        }
    }
    
}

