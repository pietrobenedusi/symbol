#include "Space_time_symbol_PC.hpp"
// #include "Space_time_assembler.hpp"
#include <math.h>       /* pow */
#include <ctime>


using namespace utopia;

PetscErrorCode SymbolShellPCCreate(SymbolShellPC **shell)
{
    SymbolShellPC  *newctx;

    PetscNew(&newctx);    
    *shell       = newctx;
    return 0;
}

// old version before petsc problems 
// PetscErrorCode SymbolShellPCSetUp(PC pc, Space_time_assembler *st_op_, const int levels, Mat *M1_, Mat *M2_, Mat *M3_)
// {
//     SymbolShellPC  *shell;
//     PetscErrorCode  ierr;
    
//     PCShellGetContext(pc,(void**)&shell);    
//     shell->st_op = st_op_;  
//     auto space_op =st_op_->get_space_op(); 

//     // set smothing steps
//     auto p = space_op->get_p();
//     shell->PCG_smoothing_steps = (p[0] < 4) ? 2 : p[0];

//     // compute interpolation operators if needed 
//     shell->interpolation_operators.reserve(levels);

//     // get space #dofs
//     int dim = space_op->get_dim();
//     int size = space_op->get_size(); // for now MG works for squre domain, not rectangular, 
//     int n_fine = (int) pow(size, 1./dim);

//     // get time #dofs
//     int time_steps = st_op_->get_time_steps();
//     int time_size = st_op_->get_time_op()->get_size();
//     int tot_time_size = time_steps * time_size;

//     PetscMatrix I_time = identity(tot_time_size,tot_time_size);

//     // set up interpolant
//     for (int i = 0; i < levels - 1; ++i)
//     {
//         Mat I_space, I;

//         // number of grid points for level l
//         int n_l = 1 + (n_fine - 1)/pow(2, levels - 2 - i);  // +2 for BC correction
        
//         ierr = assemble_interpolation(n_l, dim, I_space);CHKERRQ(ierr);

//         ierr = kron(raw_type(I_time), I_space, I);CHKERRQ(ierr);  
        
//         // store the interp. operators, coarsest is in 0 
//         shell->interpolation_operators.push_back(I);

//         ierr = MatDestroy(&I_space);CHKERRQ(ierr);        
//     }

//     // set up MG

//     // get the operators 
//     auto space_approx = st_op_->get_space_approx();
//     auto space_mass_approx = st_op_->get_space_mass_approx();

//     ierr = KSPCreate(PETSC_COMM_WORLD,&(shell->space_solver));CHKERRQ(ierr);    
//     ierr = KSPSetOperators(shell->space_solver, raw_type(*space_approx), raw_type(*space_approx));CHKERRQ(ierr);         
//     ierr = KSPSetType(shell->space_solver,KSPRICHARDSON);CHKERRQ(ierr);     
//     ierr = KSPSetUp(shell->space_solver);CHKERRQ(ierr);

//     // ierr = KSPSetTolerances(space_solver,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);           
//     ierr = KSPSetFromOptions(shell->space_solver);CHKERRQ(ierr);         

//     if (levels > 1)
//     {                            
//         // MG solver   
//         PC pcmg;
//         ierr = KSPGetPC(shell->space_solver, &pcmg);
//         ierr = PCSetType(pcmg, PCMG);    
//         ierr = PCMGSetLevels(pcmg,levels, NULL);CHKERRQ(ierr);

//         ierr = PCMGSetType(pcmg,PC_MG_KASKADE);CHKERRQ(ierr);    
//         ierr = PCMGSetGalerkin(pcmg,PC_MG_GALERKIN_MAT);CHKERRQ(ierr);        // MAT or BOTH
        
//         // smoothers    
//         for (int i = 1; i < levels; ++i)  
//         {
//             KSP smoother;
            
//             ierr = PCMGGetSmoother(pcmg, i, &smoother);CHKERRQ(ierr);

//             ierr = KSPSetType(smoother, KSPRICHARDSON);CHKERRQ(ierr); // CG?            

//             PC sm;
//             ierr = KSPGetPC(smoother, &sm);CHKERRQ(ierr);            
//             ierr = PCSetType(sm, PCSOR    );CHKERRQ(ierr);            
            
//             ierr = PCMGSetInterpolation(pcmg, i, shell->interpolation_operators[i-1]);CHKERRQ(ierr);             
//         }

//         ierr = PCMGSetNumberSmooth(pcmg, shell->PCG_smoothing_steps);CHKERRQ(ierr);
//     }    

//    return 0;
// }


PetscErrorCode SymbolShellPCSetUp(PC pc, Space_time_assembler *st_op_, const int levels, Mat *M1_, Mat *M2_, Mat *M3_)
{
    SymbolShellPC  *shell;
    PetscErrorCode  ierr;
    
    PCShellGetContext(pc,(void**)&shell);    
    shell->st_op = st_op_;  
    auto space_op = st_op_->get_space_op(); 

    // set up mass matrices for tensor solver(if provided)
    shell->M1 = M1_;  
    shell->M2 = M2_;  
    shell->M3 = M3_;  

    // set smothing steps
    auto p = space_op->get_p();
    // shell->PCG_smoothing_steps = (p[0] < 4) ? 3 : p[0];
    shell->PCG_smoothing_steps = 1;
    
    const int mu = 1;

    // compute interpolation operators if needed 
    shell->interpolation_operators.reserve(levels);

    // get space #dofs
    const int dim  = space_op->get_dim();
    const int size = space_op->get_size(); 
    const int n_fine = (int) pow(size, 1./dim);

    // get time #dofs
    const int time_steps = st_op_->get_time_steps();
    const int time_size  = st_op_->get_time_op()->get_size();
    const int tot_time_size = time_steps * time_size;

    PetscMatrix I_time = identity(tot_time_size,tot_time_size);

    // set up interpolant
    for (int i = 0; i < levels - 1; ++i)
    {
        Mat I_space, I;

        // number of grid points for level l
        int n_l = 1 + (n_fine - 1)/pow(2, levels - 2 - i);  
        
        ierr = assemble_interpolation(n_l, dim, I_space);CHKERRQ(ierr);
        

        if (i == levels - 2) // fine level: same structure of space-time operator
        {
            ierr = kron(raw_type(I_time), I_space, I, &raw_type(*st_op_->get_space_time_system()));CHKERRQ(ierr);           
        }
        else
        {
            ierr = kron(raw_type(I_time), I_space, I);CHKERRQ(ierr);                  
        }
        
        // store the interp. operators, coarsest is in 0 
        shell->interpolation_operators.push_back(I);

        ierr = MatDestroy(&I_space);CHKERRQ(ierr);          
    }

    ierr = MatDestroy(&raw_type(I_time));CHKERRQ(ierr);

    // set up MG

    // get the operators 
    auto space_approx = st_op_->get_space_approx();
    auto space_mass_approx = st_op_->get_space_mass_approx();

    ierr = KSPCreate(PETSC_COMM_WORLD,&(shell->space_solver));CHKERRQ(ierr);
    ierr = KSPSetOperators(shell->space_solver, raw_type(*space_approx), raw_type(*space_approx));CHKERRQ(ierr); 
    
    if (levels > 1)
    {    
        ierr = KSPSetType(shell->space_solver,KSPRICHARDSON);CHKERRQ(ierr); // for MG        
        ierr = KSPSetTolerances(shell->space_solver,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, mu);CHKERRQ(ierr);
        // ierr = KSPSetTolerances(shell->space_solver,1e-8,PETSC_DEFAULT,PETSC_DEFAULT, PETSC_DEFAULT);CHKERRQ(ierr);
    }
    else
    {
        ierr = KSPSetType(shell->space_solver,KSPCG);CHKERRQ(ierr);  
        ierr = KSPSetTolerances(shell->space_solver,1e-3,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
        // ierr = KSPSetTolerances(shell->space_solver,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,1);CHKERRQ(ierr);
    }
            
    // ierr = KSPSetFromOptions(shell->space_solver);CHKERRQ(ierr); 
    // ierr = KSPSetNormType(shell->space_solver,  KSP_NORM_UNPRECONDITIONED );CHKERRQ(ierr);
    // ierr = KSPSetUp(shell->space_solver);CHKERRQ(ierr); // --------------------------------------->> SETUP here, remove?

    if (levels > 1)
    {        
        // MG solver   
        PC pcmg;
        ierr = KSPGetPC(shell->space_solver, &pcmg);
        ierr = PCSetType(pcmg, PCMG);    
        ierr = PCMGSetLevels(pcmg,levels, NULL);CHKERRQ(ierr);

        ierr = PCMGSetType(pcmg,PC_MG_KASKADE);CHKERRQ(ierr);    
        // ierr = PCMGSetGalerkin(pcmg,PC_MG_GALERKIN_NONE);CHKERRQ(ierr);        
        ierr = PCMGSetGalerkin(pcmg,PC_MG_GALERKIN_BOTH);CHKERRQ(ierr);        

        // Mat K_coarse;

        // smoothers    
        // for (int i = levels - 1; i >= 0; --i)  
        for (int i = 1; i < levels; ++i) // not the coarsest
        {      
            // if (i == levels - 1) // fine level 
            // {    
            //     if (shell->M1 == NULL) // if there are no mass matrices
            //     {
            //         // CG smoother
            //         // if (mpi_world_rank() == 0) std::cout << "ILU-CG solver used on fine level with s_p = " << shell->PCG_smoothing_steps << std::endl;
            //         // ierr = KSPSetType(smoother, KSPCG);CHKERRQ(ierr);                                                       
            //         // ierr = KSPSetTolerances(smoother,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, shell->PCG_smoothing_steps);CHKERRQ(ierr);                                   
            //         // ierr = KSPSetNormType(smoother, KSP_NORM_NONE);CHKERRQ(ierr);  

            //         // G-S smoother
            //         if (mpi_world_rank() == 0) std::cout << "G-S solver used on fine level with s_p = " << shell->PCG_smoothing_steps << std::endl;
            //         ierr = KSPSetType(smoother, KSPRICHARDSON     );CHKERRQ(ierr); 
            //         // ierr = KSPRichardsonSetScale(smoother, 0.8);CHKERRQ(ierr); 

            //         PC sm;
            //         ierr = KSPGetPC(smoother, &sm);CHKERRQ(ierr);            
            //         ierr = PCSetType(sm, PCSOR         );CHKERRQ(ierr); 

            //         ierr = KSPSetTolerances(smoother,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, shell->PCG_smoothing_steps);CHKERRQ(ierr);     
            //         ierr = KSPSetNormType(smoother, KSP_NORM_NONE);CHKERRQ(ierr); 
            //     }  
            //     else // now just for 2D // TODO put warning?
            //     {
            //         if (mpi_world_rank() == 0) std::cout << "Tensor solver for mass matrix is used with s_p = " << shell->PCG_smoothing_steps << std::endl;

            //         ierr = KSPSetType(smoother, KSPCG);CHKERRQ(ierr);                     
            //         ierr = KSPSetOperators(smoother, raw_type(*space_approx), raw_type(*space_mass_approx));CHKERRQ(ierr);                                                         
            //         ierr = KSPSetTolerances(smoother,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, shell->PCG_smoothing_steps);CHKERRQ(ierr);                                                       
            //         // ierr = KSPSetNormType(smoother,  KSP_NORM_UNPRECONDITIONED );CHKERRQ(ierr);  //for debug                    
            //         ierr = KSPSetNormType(smoother, KSP_NORM_NONE);CHKERRQ(ierr);   

            //         PC tensor_solver;
            //         ierr = KSPGetPC(smoother, &tensor_solver);CHKERRQ(ierr);  

            //         // init tensor solver 
            //         TensorSolverShellPC  *tensor_shell;

            //         ierr = PCSetType(tensor_solver,PCSHELL);CHKERRQ(ierr);  
            //         ierr = TensorSolverPCCreate(&tensor_shell);CHKERRQ(ierr);                  
            //         ierr = PCShellSetApply(tensor_solver,TensorSolverPCApply);CHKERRQ(ierr);              
            //         ierr = PCShellSetContext(tensor_solver,tensor_shell);CHKERRQ(ierr);                  
            //         ierr = PCShellSetDestroy(tensor_solver,TensorSolverPCDestroy);CHKERRQ(ierr);                       
            //         ierr = TensorSolverPCSetUp(tensor_solver, shell->st_op, shell->M1, shell->M2);CHKERRQ(ierr);       
            //     }                         
            // }
            // else // coarse levels
            // {                         
            //     if ( i == levels - 2) // first time          
            //     {
            //         ierr = MatPtAP(raw_type(*space_approx), shell->interpolation_operators[i], MAT_INITIAL_MATRIX, 1., &K_coarse);CHKERRQ(ierr); 
            //     }
            //     else
            //     {
            //         ierr = MatPtAP(K_coarse, shell->interpolation_operators[i], MAT_INITIAL_MATRIX, 1., &K_coarse);CHKERRQ(ierr); 
            //     }
                
            //     ierr = KSPSetOperators(smoother, K_coarse, K_coarse);CHKERRQ(ierr);                         
            // }                            

            // if (i > 0 && i < levels - 1) // nor coarser or finer
            // {        
                // G-S smoother
                KSP smoother;
                int smothing_steps = 1;

                // ierr = PCMGGetSmootherDown(pcmg, i, &smoother);CHKERRQ(ierr);        
                ierr = PCMGGetSmoother(pcmg, i, &smoother);CHKERRQ(ierr);        

                ierr = KSPSetType(smoother, KSPRICHARDSON);CHKERRQ(ierr); 

                PC sm;
                ierr = KSPGetPC(smoother, &sm);CHKERRQ(ierr);            
                ierr = PCSetType(sm, PCSOR   );CHKERRQ(ierr);
                // ierr = PCSORSetSymmetric(sm, SOR_LOCAL_BACKWARD_SWEEP);CHKERRQ(ierr);  
                // ierr = PCSORSetSymmetric(sm, SOR_LOCAL_FORWARD_SWEEP);CHKERRQ(ierr); 


                ierr = KSPSetTolerances(smoother,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, smothing_steps);CHKERRQ(ierr);
                ierr = KSPSetNormType(smoother, KSP_NORM_NONE);CHKERRQ(ierr);                                        

                // post-smoothing
                // ierr = PCMGGetSmootherUp(pcmg, i, &smoother);CHKERRQ(ierr);        
                // ierr = KSPSetType(smoother, KSPRICHARDSON);CHKERRQ(ierr); 
                
                // ierr = KSPGetPC(smoother, &sm);CHKERRQ(ierr);            
                // ierr = PCSetType(sm, PCSOR   );CHKERRQ(ierr); 
                // ierr = PCSORSetSymmetric(sm, SOR_LOCAL_BACKWARD_SWEEP);CHKERRQ(ierr); 

                // ierr = KSPSetTolerances(smoother,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT, smothing_steps);CHKERRQ(ierr);
                // ierr = KSPSetNormType(smoother, KSP_NORM_NONE);CHKERRQ(ierr);                                        


            // }        

            // set interpolators
            // if (i > 0)
            // {            
                ierr = PCMGSetInterpolation(pcmg, i, shell->interpolation_operators[i-1]);CHKERRQ(ierr);  
            // }            
        }

        // ierr = MatDestroy(&K_coarse);CHKERRQ(ierr);        
    }

   return 0;
}

PetscErrorCode SymbolShellPCApply(PC pc, Vec x_in, Vec x_out)
{

    SymbolShellPC   *shell;
    PetscErrorCode  ierr;
    
    PCShellGetContext(pc,(void**)&shell);

    // solve: compute x_in = inv(K_h) * x_in
    // ierr = KSPSolve(shell->space_solver, x_in, x_in);CHKERRQ(ierr);
   ierr = KSPSolve(shell->space_solver, x_in, x_out);CHKERRQ(ierr);

    // clean ?
    // ierr = KSPDestroy(&space_solver);CHKERRQ(ierr);   

    // final multiplication: x_out = K * x_in  
    // auto time_approx = shell->st_op->get_time_approx();
    // ierr =  MatMult(raw_type(*time_approx), x_in, x_out);CHKERRQ(ierr);

    return 0;
}

PetscErrorCode SymbolShellPCDestroy(PC pc)
{
    SymbolShellPC *shell;

	PCShellGetContext(pc,(void**)&shell);    
    PetscFree(shell);

	return 0;
}
