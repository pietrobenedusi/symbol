#ifndef Space_time_assembler_hpp
#define Space_time_assembler_hpp

#include "Space_assembler.hpp"
#include "Time_assembler.hpp"

class Space_time_assembler{

    typedef std::shared_ptr<utopia::PetscMatrix> Matrix;
    typedef std::shared_ptr<utopia::PetscVector>  Vector;

public:
    Space_time_assembler(const std::shared_ptr<Space_assembler> space_op_, 
                         const std::shared_ptr<Time_assembler> time_op_, const int time_steps_ = 10, 
                         const bool compute_prec_ = true) :
        space_op(space_op_), 
        time_op(time_op_),
        time_steps(time_steps_){        

        // check if things have been assembled
        assert(space_ops->get_mass() && "Assemble space operators!");        
        assert(time_ops->get_mass()  && "Assemble time operators!" );
            
        init_sizes();

        // assemble system and rhs
        assemble();        
        assemble_rhs();

        compute_prec_ ? assemble_preconditioner() : MatDestroy(&raw_type(*A_prec));
        // if (!compute_prec_) MatDestroy(&raw_type(*A_prec));                    
    }


    // TODO with args p,k,q,nx,nt,dt
    // void init_space_time_assembler(){

    //     space_ops = std::make_shared<SpaceAssemblerT>(init, mesh_path);
    //     time_ops  = std::make_shared<Time_assembler>(time_step);

    //     time_steps = time_steps_;

    //     space_ops->assemble();
    //     time_ops->assemble();

    //     init_sizes();
    // }
    
    // inline void set_space_time_rhs(const utopia::PetscVector space_time_rhs_) {space_time_rhs = std::make_shared<utopia::PetscVector>(space_time_rhs_);}
    
    // getters
    inline const Matrix &get_space_time_system()      const{ return space_time_system;     }
    inline const Vector &get_space_time_rhs()         const{ return space_time_rhs;        }
    inline const Matrix &get_space_time_system_prec() const{ return space_time_system_prec;}

    inline unsigned int get_time_steps() const{ return time_steps;}
    inline utopia::SizeType get_size_A() const{ return size_A;    }
    inline utopia::SizeType   get_size() const{ return size_;     }
    
    inline std::shared_ptr<Space_assembler> get_space_op() const{ return space_op;}
    inline std::shared_ptr<Time_assembler>  get_time_op()  const{ return time_op; }

    inline Matrix get_space_approx(){ 
        
        if(!space_approx) set_space_approx();
        
        return space_approx;
    }   

    inline Matrix get_space_mass_approx(){ 
    
        if(!space_mass_approx) set_space_mass_approx();

        return space_mass_approx;
    }  

    inline Matrix get_time_approx(){ 
        
        if(!time_approx) set_time_approx();
        
        return time_approx;
    }   

    inline Matrix get_time_coupling(){ 
        
        if(!time_approx) set_time_coupling();
        
        return time_coupling;
    } 

    void set_rhs( const double value_);

    
private:
    Matrix A;  // these are freed after the system has been built 
    Matrix A_prec; // symbol based block A
    Matrix B; 
    Matrix space_time_system;
    Matrix space_time_system_prec;    
    Vector space_time_rhs;

    // matrices neede by the symbol based PGMRES 
    Matrix space_approx;       // I_t kron space_stiff 
    Matrix space_mass_approx;  // I_t kron mass_stiff 
    Matrix time_approx;        // I_t_steps kron time_mass_inv kron I_space
    Matrix time_coupling;      // I_t_steps kron time_coupling kron M_space
    
    // space and time operators
    std::shared_ptr<Space_assembler> space_op;
    std::shared_ptr<Time_assembler>  time_op;

    // sizes
    void init_sizes();
    unsigned int time_steps;
    utopia::SizeType size_A;  
    utopia::SizeType size_; // size of space_time_system = (size_A * time_steps)
    
    // assembling routines    
    PetscErrorCode assemble();       
    PetscErrorCode assemble_rhs();
    PetscErrorCode assemble_A_B_blocks();
    PetscErrorCode assemble_preconditioner();  

    //setters
    void set_space_approx();
    void set_space_mass_approx();
    void set_time_approx(); 
    void set_time_coupling();  
};

#endif 

