#include "Tensor_solverPC.hpp"

using namespace utopia;

PetscErrorCode TensorSolverPCCreate(TensorSolverShellPC **shell)
{
    TensorSolverShellPC  *newctx;

    PetscNew(&newctx);    
    *shell       = newctx;
    return 0;
}

// set up solver
PetscErrorCode TensorSolverPCSetUp(PC pc, Space_time_assembler *st_op_, Mat *M1_, Mat *M2_)
{
    TensorSolverShellPC  *shell;
    PetscErrorCode  ierr;

    const double a_tol = 1e-3;    
    
    // set up the structure
    PCShellGetContext(pc,(void**)&shell);    
    shell->st_op = st_op_;      

    PetscInt size_M1_, size_M2_;
    ierr = MatGetSize(*M1_, &size_M1_, NULL);CHKERRQ(ierr);
    ierr = MatGetSize(*M2_, &size_M2_, NULL);CHKERRQ(ierr);
    
    // get sizes
    int time_steps = st_op_->get_time_steps();
    int time_size = st_op_->get_time_op()->get_size();
    
    time_size = time_steps * time_size;

    // identities
    PetscMatrix I_m1 = identity(size_M1_,size_M1_);
    PetscMatrix I_m2 = identity(size_M2_,size_M2_);
    PetscMatrix I_time = identity(time_size,time_size);
    
    Mat M_tmp1, M_tmp2;

    // I2 kron M1
    ierr = kron(*M1_, raw_type(I_m2), M_tmp1);CHKERRQ(ierr);   
    ierr = kron(raw_type(I_time), M_tmp1, shell->st_M1, &raw_type(*st_op_->get_space_time_system()));CHKERRQ(ierr);   
    
    // I1 kron M2
    ierr = kron(raw_type(I_m1), *M2_, M_tmp2);CHKERRQ(ierr);       
    ierr = kron(raw_type(I_time), M_tmp2, shell->st_M2, &raw_type(*st_op_->get_space_time_system()));CHKERRQ(ierr);   
    
    //set up solvers 
    ierr = KSPCreate(PETSC_COMM_WORLD,&(shell->solver1));CHKERRQ(ierr);    
    ierr = KSPSetOperators(shell->solver1, shell->st_M2, shell->st_M2);CHKERRQ(ierr); 
    ierr = KSPSetTolerances(shell->solver1,a_tol,PETSC_DEFAULT,PETSC_DEFAULT, PETSC_DEFAULT);CHKERRQ(ierr);
    ierr = KSPSetUp(shell->solver1);CHKERRQ(ierr);
    
    ierr = KSPCreate(PETSC_COMM_WORLD,&(shell->solver2));CHKERRQ(ierr);    
    ierr = KSPSetOperators(shell->solver2, shell->st_M1, shell->st_M1);CHKERRQ(ierr); 
    ierr = KSPSetType(shell->solver2,KSPCG);CHKERRQ(ierr); 
    ierr = KSPSetTolerances(shell->solver2,a_tol,PETSC_DEFAULT,PETSC_DEFAULT, PETSC_DEFAULT);CHKERRQ(ierr);
    ierr = KSPSetUp(shell->solver2);CHKERRQ(ierr);

    // clean
    ierr = MatDestroy(&M_tmp1);CHKERRQ(ierr);
    ierr = MatDestroy(&M_tmp2);CHKERRQ(ierr);
    ierr = MatDestroy(&raw_type(I_m1));CHKERRQ(ierr);
    ierr = MatDestroy(&raw_type(I_m2));CHKERRQ(ierr);
    ierr = MatDestroy(&raw_type(I_time));CHKERRQ(ierr);

    return ierr;
}

PetscErrorCode TensorSolverPCApply(PC pc,Vec x_in,Vec x_out)
{
    TensorSolverShellPC   *shell;
    PetscErrorCode  ierr;
    
    PCShellGetContext(pc,(void**)&shell);

    // first solve
    ierr = KSPSolve(shell->solver1, x_in, x_out);CHKERRQ(ierr);
 
    // second one
    ierr = KSPSolve(shell->solver2, x_out, x_out);CHKERRQ(ierr);   
     
    return ierr;
}

PetscErrorCode TensorSolverPCDestroy(PC pc)
{
    TensorSolverShellPC *shell;

	PCShellGetContext(pc,(void**)&shell);    
    PetscFree(shell);

	return 0;
}
