#ifndef Space_assembler_hpp
#define Space_assembler_hpp

#include "utopia.hpp"
#include "petiga.h"
#include "Utilities.hpp"

// Assemble the IgA (p,k) matrices for the space problem in (0,1)^d with n^d grid points
 
class Space_assembler{

    typedef std::shared_ptr<utopia::PetscMatrix> 	Matrix;
    typedef std::shared_ptr<utopia::PetscVector>  	Vector;    
    typedef std::vector<unsigned int>   	   	Array;    
    typedef std::vector<PetscReal>   	   	   	Point;
    typedef std::vector<std::vector<PetscReal>> Coefficients;  
 
public:
    // constructor
    Space_assembler(const Array p, const Array k, const Array n_per_dim, double (*f)(Point), double (*jacobian_det)(Point), Coefficients (*coeff)(Point), bool mass_lumping = false, const bool bc_flag = true):
    bc_flag_(bc_flag), p_(p), k_(k), n_per_dim_(n_per_dim), f_(f), jacobian_det_(jacobian_det), coeff_(coeff), mass_lumping_(mass_lumping)
    {
        dim_ = p_.size();
            
    	if (k_.size() !=  dim_ || n_per_dim_.size() !=  dim_)
        {
            std::cerr << "ERROR in Space_assembler constructor: mismatching vector length!" << std::endl;        
    	} 

        assemble();     
    }

    // getters
    inline Matrix get_mass()  const{ return mass_ ; }
    inline Matrix get_stiff() const{ return stiff_; }
    inline Vector get_rhs()   const{ return rhs_  ; }
    inline int	  get_dim()   const{ return dim_  ; }
    inline int    get_size()  const{ return size_ ; }

    inline Array  get_n_per_dim_()  const{ return n_per_dim_; }
    inline Array  get_p()  const{ return p_; }
    
private:

    // parameters
    bool 			bc_flag_;  // true to eliminate of rows and cols for boundary conditions     
    Array 			p_;        // order of basis
    Array 			k_;        // regularity 
    Array 			n_per_dim_;   // number of unknowns per dimension 
    unsigned int	dim_;         // number of dimensions 
	int 			size_;  
	double 			(*f_)(Point);     // rhs
    double          (*jacobian_det_)(Point);     // absolute value of the Jacobian determinant for geometry transformation
	Coefficients 	(*coeff_)(Point); // diffusion
    bool            mass_lumping_;
	
    // operators  
    Matrix  mass_;
    Matrix stiff_;
    Vector   rhs_;

    // assemble operators 
    PetscErrorCode assemble();
};

#endif 
