#ifndef Utilities_hpp
#define Utilities_hpp

#include "utopia.hpp"

// output
void print_local_sizes(const Mat &M);
void print_global_sizes(const Mat &M);

void save_mat(Mat &m, const char * filename, const char * name);
void save_vec(Vec &m, const char * filename, const char * name);

// this is needed for preallocation
int count_max_nz(const Mat &A);

std::vector<int> generate_distribution(const int size_A, const int size);

// tensor product 
PetscErrorCode kron(const Mat &A, const Mat &B_in, Mat &AB, Mat* AB_structure = NULL);

PetscErrorCode assemble_interpolation(const int n_fine, const int dim, Mat &I);
PetscErrorCode coupling_even(const int time_steps_fine, Mat &I);
// void coupling_q(const int time_nodes_fine, utopia::PetscMatrix &interp_op);

PetscErrorCode make_toeplitz(Mat &A, const int p);


void print_KSP_infos(const KSP &solver);

#endif 

