#ifndef Time_assembler_hpp
#define Time_assembler_hpp

#include "utopia.hpp"
#include "utopia_DeprecatedHeaders.hpp"

// Assemble the time matrices using basis of order q using Gauss-Radau points 
 
class Time_assembler{

    typedef std::shared_ptr<utopia::PetscMatrix> Matrix;
     
public:

    // constructor
    Time_assembler(const double time_step, const int q = 1) : time_step_(time_step), q_(q), size_(q+1) {

        if (q < 0 || q > 4)
        {
            std::cerr << "ERROR: Order q should be in [0,4]!" << std::endl;
        }
        
        assemble();
    }

    // getters
    inline int get_size() const{ return size_;}
    inline double get_time_step() const{ return time_step_; }

    inline Matrix get_mass()     const{ return mass_;     }
    inline Matrix get_mass_inv() const{ return mass_inv;  }
    inline Matrix get_stiff()    const{ return stiff_;    }
    inline Matrix get_coupling() const{ return coupling_; }
            
private:

    // parameters
    double time_step_;   // dt
    unsigned int q_;     // order of basis
    unsigned int size_;  // q + 1
    
    // operators 
    Matrix  mass_;
    Matrix  mass_inv;
    Matrix  stiff_; // H + C in the paper
    Matrix  coupling_;  // J in the paper

    // assemble operators 
    void assemble();
};

#endif 
