#ifndef Tensor_PC_hpp
#define Tensor_PC_hpp

#include "Utilities.hpp"
#include "Space_time_assembler.hpp"

typedef struct {
	
	Space_time_assembler *st_op; 	
	
	Mat st_M1;
	Mat st_M2;

	KSP solver1;     
	KSP solver2;     

} TensorSolverShellPC;

extern PetscErrorCode TensorSolverPCCreate(TensorSolverShellPC **shell);
extern PetscErrorCode TensorSolverPCSetUp(PC pc, Space_time_assembler *st_op_, Mat *M1_, Mat *M2_);
extern PetscErrorCode TensorSolverPCApply(PC pc,Vec x,Vec y);
extern PetscErrorCode TensorSolverPCDestroy(PC pc);

#endif 
